'use strict';

//handler for tile, added pressed state
(function() {
  var main = document.getElementsByTagName('main')[0];
  
  if ('ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch) {
    main.addEventListener('touchstart', onTouchStart);
  }
  main.addEventListener('mousedown', onMouseDown);
  
  function onTouchStart(event) {
    var tile = getTile(event.target);
    if (!tile) return;
    tile.addEventListener('touchend', onTouchEnd);
    addEffect(event.touches[0].clientX, event.touches[0].clientY, tile);
  }
  function onMouseDown(event) {
    var tile = getTile(event.target);
    if (!tile) return;
    tile.addEventListener('mouseup', onMouseUp);
    tile.addEventListener('mouseout', onMouseUp);
    addEffect(event.clientX, event.clientY, tile);
  }
  function onMouseUp(event) {
    var tile = getTile(event.target);
    if (!tile) return;
    tile.classList.remove('pressed-right', 'pressed-left', 'pressed-top', 'pressed-bottom');
    tile.removeEventListener('mouseup', onMouseUp);
    tile.removeEventListener('mouseout', onMouseUp);
  }
  function onTouchEnd(event) {
    var tile = getTile(event.target);
    if (!tile) return;
    tile.classList.remove('pressed-right', 'pressed-left', 'pressed-top', 'pressed-bottom');
    tile.removeEventListener('touchend', onTouchEnd);
  }
  function getTile(target) {
    while (target != main) {
      if (target.classList.contains('tile')) return target;
      target = target.parentNode;
    }
    return false;
  }
  function addEffect(x, y, target) {
    var rect = target.getBoundingClientRect(),
        topSpace = y - rect.top,
        bottomSpace = rect.bottom - y,
        leftSpace = x - rect.left,
        rightSpace = rect.right - x,
        minSpace = Math.min(topSpace, bottomSpace, leftSpace, rightSpace),
        className = '';
    if (minSpace == topSpace) className = 'pressed-top';
    if (minSpace == bottomSpace) className = 'pressed-bottom';
    if (minSpace == leftSpace) className = 'pressed-left';
    if (minSpace == rightSpace) className = 'pressed-right';
    target.classList.add(className);
  }
})();

//component for display newsfeed from news.json
function NewsFeed(options) {
  this.title = document.querySelector(options.title) || false,
  this.text = document.querySelector(options.text) || false,
  this.date = document.querySelector(options.date) || false;
}
NewsFeed.prototype.getJson = function(url) {
  var self = this,
      xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.send();
  xhr.onreadystatechange = function() {
    if (this.readyState != 4) return;
    if (this.status != 200) {
      self.show({header: 'error', body: this.status ? this.statusText : 'request failed', date: new Date()});
      return;
    }
    buildMonths(this.responseText);
  }
  function buildMonths(response) {
    var json = JSON.parse(response);
    if (!json.news) return;
    var months = [];
    json.news.forEach(function(item) {
      var date = new Date(item.date),
          month = date.getMonth();
      if (!months[month]) months[month] = [];
      months[month].push(item);
    });
    monthCounter(months)();
  }
  function monthCounter(months) {
    var monthsLength = months.length,
        monthsCount = monthsLength - 1;
    return function counter() {
      var shownNews = [],
          news = months[monthsCount],
          rndArray = rndArrayGenerate(news.length),
          i = rndArray.length - 1;
      newsCounter();
    
      function newsCounter() {
        if (i + 1) {
          self.show(news[rndArray[i]]);
          i--;
          setTimeout(newsCounter, 15000);
          return;
        }
        monthsCount > 0 ? monthsCount-- : monthsCount = monthsLength - 1;
        setTimeout(counter, 15000);
      }
    }
  }
}
NewsFeed.prototype.show = function(news) {
  if (this.title) this.title.innerHTML = news.header;
  if (this.text) this.text.innerHTML = news.body;
  if (this.date) {
    var date = new Date(news.date),
        year = date.getFullYear(),
        month = to3char(date.getMonth()),
        day = to2char(date.getDate());
    this.date.innerHTML = month + ' ' + day + ', ' + year;
  }
}
function to2char(d) {
  if (d.toString().length < 2) return '0' + d;
  return d;
}
function to3char(m) {
  var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Nov', 'Dec'];
  return months[m];
}
function rndArrayGenerate(length) {
  var tempArray = [],
      rndArray = [],
      tempRnd;
  while (length--) tempArray.push(length);
  while (tempArray.length) {
    tempRnd = Math.floor(Math.random() * tempArray.length);
    rndArray.push(tempArray[tempRnd]);
    tempArray.splice(tempRnd, 1);
  }
  return rndArray;
}